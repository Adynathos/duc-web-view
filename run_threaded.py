#!python3
import http.server
from socketserver import ThreadingMixIn

class ThreadingHTTPServer(ThreadingMixIn, http.server.HTTPServer):
    pass

class MyHandler(http.server.CGIHTTPRequestHandler):
    cgi_directories = ['/scans']

PORT = 5335

with ThreadingHTTPServer(("", PORT), MyHandler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
