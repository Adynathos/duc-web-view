#!/usr/bin/env python3

import os
import shutil
from datetime import datetime

TRANSFER_LIST = [
	('/cvlabdata1/cvlabdata1-scan.duc.db', '/tmp/cvlabdata1-scan.duc.db'),
	('/cvlabdata2/cvlabdata2-scan.duc.db', '/tmp/cvlabdata2-scan.duc.db'),
	('/cvlabsrc1/cvlabsrc1-scan.duc.db', '/tmp/cvlabsrc1-scan.duc.db'),
]

def get_mod_time(path):
	try:
		mod_time = os.path.getmtime(path)
	except FileNotFoundError:
		mod_time = 0

	return datetime.fromtimestamp(mod_time)

def update_scan(fname_pair):
	src_file, dest_file = fname_pair

	mod_time_src, mod_time_dest = map(get_mod_time, [src_file, dest_file])
	mod_diff = mod_time_src - mod_time_dest

	print(f'{src_file} is newer than {dest_file} by {mod_diff}')

	if mod_diff.total_seconds() > 0:
		print('Copying')
		shutil.copy(src_file, dest_file)
	else:
		print('Up to date')

def main():
	for pair in TRANSFER_LIST:
		update_scan(pair)

main()
